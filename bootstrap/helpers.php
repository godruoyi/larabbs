<?php

if (! function_exists('route_class')) {
    /**
     * Get current route name
     *
     * @return string
     */
    function route_class(): string
    {
        return str_replace('.', '-', Route::currentRouteName());
    }
}


if (! function_exists('make_excerpt')) {
    /**
     * make excerpt
     *
     * @param  string      $body
     * @param  int|integer $length
     *
     * @return string
     */
    function make_excerpt(string $body, int $length = 200): string
    {
        $excerpt = trim(preg_replace('/\r\n|\r|\n+/', ' ', strip_tags($body)));

        return str_limit($excerpt, $length);
    }
}
