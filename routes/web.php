<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('root')->get('/', 'PagesController@root');

// Authentication Routes...
Route::name('login')->get('login', 'Auth\LoginController@showLoginForm');
Route::name('login.submit')->post('login', 'Auth\LoginController@login');
Route::name('logout')->post('logout', 'Auth\LoginController@logout');

// Registration Routes...
Route::name('register')->get('register', 'Auth\RegisterController@showRegistrationForm');
Route::name('register.submit')->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::name('password.request')->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::name('password.email')->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::name('password.reset')->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::name('password.submit')->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::resource('users', 'UserController', ['only' => ['show', 'update', 'edit']]);

Route::resource('topics', 'TopicsController', ['only' => ['index', 'create', 'store', 'update', 'edit', 'destroy']]);
Route::name('topics.show')->get('topics/{topic}/{sulg?}', 'TopicsController@show');

Route::resource('categories', 'CategoriesController', ['only' => 'show']);

Route::name('upload.forsimditor')->post('upload/forsimditor', 'UploadController@forsimditor');

Route::resource('replies', 'RepliesController', ['only' => ['store', 'destroy']]);

Route::resource('notifications', 'NotificationsController', ['only' => ['index']]);
