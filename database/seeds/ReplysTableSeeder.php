<?php

use Illuminate\Database\Seeder;
use App\Models\Reply;
use App\Models\User;
use App\Models\Topic;
use Faker\Generator;

class ReplysTableSeeder extends Seeder
{
    public function run()
    {
        $topicIds = Topic::all()->pluck('id')->toArray();

        $userIds = User::all()->pluck('id')->toArray();

        $faker = app(Generator::class);

        $replys = factory(Reply::class)
            ->times(100)
            ->make()
            ->each(function ($reply, $index) use ($userIds, $topicIds, $faker) {
                $reply->content = $faker->sentence();
                $reply->user_id = $faker->randomElement($userIds);
                $reply->topic_id = $faker->randomElement($topicIds);
            });

        Reply::insert($replys->toArray());
    }
}
