<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Pages Index
     *
     * @return \Illuminate\Http\Response
     */
    public function root()
    {
        return view('pages.root');
    }
}
