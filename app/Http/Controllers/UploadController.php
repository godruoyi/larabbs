<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Handlers\ImageUploadHandler;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{
    /**
     * ImageUploadHandler Instance
     *
     * @var /App\Handlers\ImageUploadHandler
     */
    protected $handler;

    /**
     * Register Image Uploader
     *
     * @param ImageUploadHandler $handler
     */
    public function __construct(ImageUploadHandler $handler)
    {
        $this->handler = $handler;

        $this->middleware('auth');
    }

    /**
     * Image upload for simditor
     *
     * @param  Request $request
     *
     * @return JsonResponse
     */
    public function forsimditor(Request $request)
    {
        $prefix = $request->user()->id;
        if ($path = $this->handler->save($request->simditor, 'topics', $prefix, 1024)) {
            return response()->json([
                'success'   => true,
                'msg'       => 'success!',
                'file_path' => $path
            ]);
        }

        return response()->json(['success' => false, 'msg' => '上传失败!', 'file_path' => '']);
    }
}
