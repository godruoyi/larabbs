<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReplyRequest;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(ReplyRequest $request, Reply $reply)
    {
        $reply->fill($request->all());
        $reply->user_id = $request->user()->id;
        $reply->topic_id = $request->topic_id;
        $reply->save();

        return redirect()->to($reply->topic->sulgLink())->with('message', 'Created successfully.');
    }

    public function destroy(Reply $reply)
    {
        $this->authorize('destroy', $reply);
        $reply->delete();

        return redirect()->to($reply->topic->sulgLink())->with('message', 'Deleted successfully.');
    }
}
