<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|between:3,25|regex:/^[A-Za-z0-9\-\_]+$/|unique:users,name,' . $this->user()->id,
            'email' => 'required|email|unique:users,email,' . $this->user()->id,
            'introduction' => 'max:80',
            'avatar' => 'sometimes|required|mimes:jpeg,bmp,png,gif|dimensions:min_width=200,min_height=200'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'introduction' => '个人简介',
            'avatar' => 'touxiang'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'     => '用户名不能为空',
            'name.between'      => '用户名必须介于 3- 25 个字符之间',
            'name.regex'        => '用户名只支持中英文、数字、下划线及横杆',
            'name.unique'       => '用户名已被占用',
            'avatar.dimensions' => '图片的清晰度不够，宽和高需要 200px 以上',
        ];
    }
}
