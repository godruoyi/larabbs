<?php

namespace App\Jobs;

use App\Models\Topic;
use Illuminate\Bus\Queueable;
use App\Handlers\TranslateHandler;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TranslateSlug implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Topic instance
     *
     * @var \App\Models\Topic
     */
    protected $topic;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Topic $topic)
    {
        $this->topic = $topic;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $transResult = app(TranslateHandler::class)->trans($this->topic->title, 'zh-CHS', 'EN');

        if ($transResult == $this->topic->title) {
            $sulg = pinyin_permalink($this->topic->title);
        } else {
            $sulg = str_slug(strtolower($transResult));
        }

        \DB::table('topics')->where('id', $this->topic->id)->update([
            'sulg' => $sulg
        ]);
    }
}
