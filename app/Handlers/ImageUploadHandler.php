<?php

namespace App\Handlers;

use Image;
use Illuminate\Http\UploadedFile;

class ImageUploadHandler
{
    protected $allowedExt = ['png', 'jpg', 'gif', 'jpeg'];

    /**
     * Save upload image
     *
     * @param  UploadedFile|null $file
     * @param  string       $folder
     * @param  string       $filePrefix
     *
     * @return string
     */
    public function save($file, string $folder, string $filePrefix, $maxWidth = false): string
    {
        if (is_null($file) || ! $file instanceof UploadedFile) {
            return false;
        }

        $folders = join(DIRECTORY_SEPARATOR, [$folder, date('Ym', time()), date('d', time())]);

        $extension = strtolower($file->getClientOriginalExtension()) ?: 'png';

        if (! in_array($extension, $this->allowedExt, true)) {
            return false;
        } elseif ($maxWidth && $extension != 'gif') {
            $this->resize($file, $maxWidth);
        }

        $filename = $filePrefix . '_' . time() . '_' . str_random(10) . '.' . $extension;

        $path = $file->storeAs($folders, $filename, 'public');

        return join(DIRECTORY_SEPARATOR, [config('app.url'), 'images', $path]);
    }

    /**
     * Resize Image width with Max Width
     *
     * @param  UploadedFile $file
     * @param  int|bool       $maxWidth
     *
     * @return null
     */
    public function resize(UploadedFile $file, $maxWidth)
    {
        Image::make($file)->resize($maxWidth, null, function ($constraint) {
            $constraint->aspectRatio();

            $constraint->upsize();
        })->save();
    }
}
