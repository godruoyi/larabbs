<?php

namespace App\Handlers;

use GuzzleHttp\Client;

class TranslateHandler
{
    const API_HTTPS = 'https://openapi.youdao.com/api';

    protected $appId;

    protected $secret;

    protected $keyword;

    public function __construct(string $appId = null, string $secret = null)
    {
        $this->appId = is_null($appId) ? config('services.translate.youdao.appid') : '';
        $this->secret = is_null($secret) ? config('services.translate.youdao.key') : '';
    }

    /**
     * Trans
     *
     * @param  string $keyword
     * @param  string $form
     * @param  string $to
     *
     * @return string
     */
    public function trans(string $keyword, string $form = 'EN', string $to = 'zh-CHS'): string
    {
        $this->keyword = $keyword;

        if (! $this->appId || ! $this->secret) {
            return $this->keyword;
        }

        try {
            return $this->toString($this->getHttpClient()->get(self::API_HTTPS, $this->buildRequestParam($keyword, $form, $to)));
        } catch (Exception $e) {
            return $keyword;
        }
    }

    /**
     * Response to string
     *
     * @param  mixed $response
     *
     * @return string
     */
    public function toString($response)
    {
        if ($response instanceof \GuzzleHttp\Psr7\Response) {
            $response = json_decode((string) $response->getBody(), true);
            if (json_last_error() === JSON_ERROR_NONE && ! empty($response['translation'][0])) {
                return $response['translation'][0];
            }
        }

        return $this->keyword;
    }

    /**
     * Get Http Client Instance
     *
     * @return \GuzzleHttp\Client
     */
    protected function getHttpClient()
    {
        return new Client();
    }

    /**
     * Build request params
     *
     * @param  string $keyword
     * @param  string $form
     * @param  string $to
     *
     * @return array
     */
    protected function buildRequestParam(string $keyword, string $form = 'en', string $to = 'zh'): array
    {
        return [
            'query' => [
                'q'     => $keyword = $this->toUTF8($keyword),
                'from'  => $form,
                'to'    => $to,
                'appKey' => $this->appId,
                'salt'  => $salt = mt_rand(10086, 99999),
                'sign'  => $this->sign($keyword, $salt)
            ],
            'verify'=> false,
        ];
    }

    /**
     * To utp-8 for gieved char
     *
     * @param  string $word
     *
     * @return string
     */
    protected function toUTF8(string $word)
    {
        return mb_convert_encoding($word, 'UTF-8', 'auto');
    }

    /**
     * @param  string $keyword
     * @param  string $salt
     *
     * @return string
     */
    protected function sign(string $keyword, string $salt): string
    {
        return md5(join('', [
            $this->appId,
            $keyword,
            $salt,
            $this->secret,
        ]));
    }
}
