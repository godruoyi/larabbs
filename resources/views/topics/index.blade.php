@extends('layouts.app')

@section('title', isset($category) ? $category->name : 'Topics Index')

@section('content')

<div class="row">
    <div class="col-lg-9 col-md-9 topic-list">

        @if (isset($category) and ! empty($category))
            <div class="alert alert-info" role="alert">
                {{ $category->name }} ：{{ $category->description }}
            </div>
        @endif

        <div class="panel panel-default">

            <div class="panel-heading">
                <ul class="nav nav-pills">
                    <li class="{{ active_class((! if_query('order', 'recent') )) }}" role="presentation"><a href="{{ Request::url() }}">Last review</a></li>
                    <li class="{{ active_class(if_query('order', 'recent')) }}" role="presentation"><a href="{{ Request::url() }}?order=recent">Last created</a></li>
                </ul>
            </div>

            <div class="panel-body">
                {{-- 话题列表 --}}
                @include('topics._topic_list', ['topics' => $topics])
                {{-- 分页 --}}
                {!! $topics->render() !!}
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 sidebar">
        @include('topics._sidebar')
    </div>
</div>

@endsection
