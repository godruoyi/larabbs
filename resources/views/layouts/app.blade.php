<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Larabbs') - Laravel BBS ~_-</title>
    <meta name="description" content="@yield('description', 'LaraBBS 爱好者社区')" />

    <link rel="stylesheet" href="{{ asset('css/app.css', true) }}">
    @yield('styles')
</head>
<body>
    <div id="app" class="{{ route_class() }}-page">
        @include('layouts._header')

        <div class="container">

            @include('layouts._message')

            @yield('content')

        </div>

        @include('layouts._footer')
    </div>

    <script type="text/javascript" src="{{ asset('js/app.js', true) }}"></script>
    @yield('scripts')
</body>
</html>
